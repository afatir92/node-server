// Import dependencies
// const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file
// const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
// mongoose.connect(dbHost);

// create mongoose schema
/*const userSchema = new mongoose.Schema({
  name: String,
  age: Number
});*/

// create mongoose model
// const User = mongoose.model('User', userSchema);
const User = [{id: 1, name: 'Jack', age: 20}]; 

/* GET api listing. */
router.get('/', (req, res) => {
        res.send('api works');
});

/* GET all users. */
router.get('/users', (req, res) => {
    /*User.find({}, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(200).json(users);
    });*/
    res.status(200).json(User);
});

/* GET one users. */
router.get('/users/:id', (req, res) => {
    /*User.findById(req.param.id, (err, users) => {
        if (err) res.status(500).send(error)

        res.status(200).json(users);
    });*/

    User.map(val => {
    	console.log(val);
    	if(req.param.id === val.id)
    		res.status(200).json(val);
    	else
    		res.status(500).send('User not found');
    });

    /*User.forEach(val => {
    	console.log(val);
    	if(req.param.id === val.id)
    		res.status(200).json(val);
    	else
    		res.status(500).send('User not found');
    });*/
});

/* Create a user. */
router.post('/users', (req, res) => {
    let user = {
        name: req.body.name,
        age: req.body.age
    };

    User.push(user);

    /*user.save(error => {
        if (error) res.status(500).send(error);

        res.status(201).json({
            message: 'User created successfully'
        });
    });*/

    res.status(201).json({
    	message: 'User created successfully'
    });
});

module.exports = router;
